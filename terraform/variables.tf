variable "name" {
  type        = string
  description = "The name of the website. It's used as the bucket name."
  default     = "pet-site"
}

variable "environment" {
  type        = string
  description = "The environment name where the website should be deployed."
}

variable "pet_name_length" {
  type        = number
  description = "The number of words for the pet name."
  default     = 2
}