# terraform-pet-site-module

This project deploys a Terraform module called `pet-site`
to the Infrastructure Registry of this project.

The module can be used using the standard Terraform `module` block:

```hcl
module "my_module_name" {
  source = "gitlab.com/timofurrer/pet-site/aws"
  version = "1.0.0"

  # variables, see below
}
```

The module supports the following variables:

| Variable        | Required | Default    | Description                                                |
|-----------------|----------|------------|------------------------------------------------------------|
| environment     | yes      |            | The environment name where the website should be deployed. |
| name            | no       | "pet-site" | The name of the website. It's used as the bucket name.     |
| pet_name_length | no       | 2          | The number of words for the pet name.                      |

A full module configuration may look like this:

```hcl
module "my_module_name" {
  source = "gitlab.com/timofurrer/pet-site/aws"
  version = "1.0.0"

  # Input variables
  environment     = "production"
  name            = "timos-pet"
  pet_name_length = 3
}
```